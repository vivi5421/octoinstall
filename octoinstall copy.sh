#!/bin/bash -xe

# Install OctoPrint
# https://makenotes.de/2019/09/octoprint-on-ubuntu-with-systemd/

HOSTNAME=octopi
USER=octoprint
MOUNT=thoster.local:/home/nas/octoprint
PYTHON=python2

hostname=$HOSTNAME
user=$USER
mount=$MOUNT
home=/home/$user
nas=$home/.nas
nasSystemDTmp=${nas:1}
nasSystemDFile=${nasSystemDTmp//\//-}.mount
venv=$home/.venv


passwd
sudo apt update && sudo apt -y upgrade
sudo apt install -y python3 python3-pip python3-dev python3-setuptools python3-venv python python-pip python-dev python-setuptools python-venv git build-essential
sudo adduser --system --shell /bin/bash --group --disabled-password --home /home/$user $user
sudo usermod -a -G tty $user
sudo usermod -a -G dialout $user
sudo -i -u $user bash << EOF
${PYTHON} -m venv ${venv}
. ${venv}/bin/activate
${PYTHON} -m pip install octoprint
EOF







echo "[Unit]
Description=Wait for Network to be Online
Documentation=man:systemd.service(5) man:systemd.special(7)
Conflicts=shutdown.target
After=network.target
Before=network-online.target

[Service]
Type=oneshot
ExecStart= \
    /bin/bash -c ' \
    if [ -e /etc/systemd/system/dhcpcd.service.d/wait.conf ]; \
    then \
        echo Wait for Network: enabled; \
        while [ -z $(hostname --all-fqdns) ]; \
        do \
            sleep 1; \
        done; \
    else \
        echo Wait for Network: disabled; \
        exit 0; \
    fi'
TimeoutStartSec=1min 30s

[Install]
WantedBy=network-online.target
" | sudo tee /etc/systemd/system/network-wait-online.service






echo "
[Unit]
Description = Mount NFS Share
Requires=network-wait-online.service
After=network-wait-online.service

[Mount]
What=${mount}
Where=${nas}
Type=nfs
Options=nfsvers=3
# Uncomment the below if your server is real slow
# TimeoutSec=10

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/$nasSystemDFile








echo "
[Unit]
Description=OctoPrint - Open Source Printing Interface for 3D Printers
Documentation=https://docs.octoprint.org
Requires=${nasSystemDFile}
After=${nasSystemDFile}

[Service]
User=${user}
Environment=HOME=${home}
WorkingDirectory=${home}
ExecStart=${venv}/bin/${PYTHON} ${venv}/bin/octoprint --basedir ${home} --config ${home}/config.yaml --port 5000 serve
Restart=always
RestartSec=100

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/octoprint.service


sudo ln -fs $nas/timelapse $home/
sudo ln -fs $nas/uploads $home/



sudo systemctl daemon-reload
sudo systemctl enable --now network-wait-online.service
sudo systemctl enable --now $nasSystemDFile
sudo systemctl enable --now octoprint.service

systemctl status octoprint.service







echo "
# octoprint ALL = NOPASSWD: /bin/systemctl start octoprint.service
# octoprint ALL = NOPASSWD: /bin/systemctl stop octoprint.service
${user} ALL = NOPASSWD: /bin/systemctl restart octoprint.service
# octoprint ALL = NOPASSWD: /bin/systemctl reboot
# octoprint ALL = NOPASSWD: /bin/systemctl poweroff
" | sudo tee /etc/sudoers.d/$USER




echo "${hostname}" | sudo tee /etc/hostname
echo "127.0.0.1    ${hostname}" | sudo tee -a /etc/hosts
