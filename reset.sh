USER=octoprint
HOSTNAME=octopi

HOME=/home/$USER

sudo systemctl stop octoprint
sudo systemctl disable octoprint
sudo systemctl stop home-octoprint-mnt.mount
sudo systemctl disable home-octoprint-mnt.mount
sudo systemctl stop network-wait-online
sudo systemctl disable network-wait-online
sudo deluser $USER
sudo rm -rf $HOME
sudo rm /etc/systemd/system/network-wait-online.service
sudo rm /etc/systemd/system/home-octoprint-mnt.mount
sudo rm /etc/systemd/system/octoprint.service