#!/bin/bash -xe

# Install OctoPrint
# https://makenotes.de/2019/09/octoprint-on-ubuntu-with-systemd/



NAS=thoster.local:/home/nas/octoprint
MOUNTPOINT=$HOME/.nas






echo "[Unit]
Description=Wait for Network to be Online
Documentation=man:systemd.service(5) man:systemd.special(7)
Conflicts=shutdown.target
After=network.target
Before=network-online.target

[Service]
Type=oneshot
ExecStart= \
    /bin/bash -c ' \
    if [ -e /etc/systemd/system/dhcpcd.service.d/wait.conf ]; \
    then \
        echo Wait for Network: enabled; \
        while [ -z $(hostname --all-fqdns) ]; \
        do \
            sleep 1; \
        done; \
    else \
        echo Wait for Network: disabled; \
        exit 0; \
    fi'
TimeoutStartSec=1min 30s

[Install]
WantedBy=network-online.target
" | sudo tee /etc/systemd/system/network-wait-online.service






echo "
[Unit]
Description = Mount NFS Share
Requires=network-wait-online.service
After=network-wait-online.service

[Mount]
What=${NAS}
Where=/home/pi/.nas
Type=nfs
Options=nfsvers=3
# Uncomment the below if your server is real slow
# TimeoutSec=10

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/home-pi-.nas.mount







echo "
[Unit]
Description=OctoPrint - Open Source Printing Interface for 3D Printers
Documentation=https://docs.octoprint.org
Requires=home-pi-.nas.mount
After=home-pi-.nas.mount

[Service]
User=octoprint
Environment=HOME=$HOME
WorkingDirectory=$HOME
ExecStart=/home/pi/oprint/bin/octoprint --host=127.0.0.1 --port=5000 serve
Restart=always
RestartSec=100

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/octoprint.service


sudo systemctl disable octoprint
sudo rm /etc/init.d/octoprint

sudo systemctl daemon-reload
sudo systemctl enable --now network-wait-online.service

mkdir $MOUNTPOINT
sudo systemctl enable --now home-pi-.nas.mount

rm -rf /home/pi/.octoprint/timelapse
sudo ln -fs $MOUNTPOINT/timelapse $HOME/.octoprint/timelapse
rm -rf /home/pi/.octoprint/uploads
sudo ln -fs $MOUNTPOINT/uploads $HOME/.octoprint/uploads

sudo systemctl enable --now octoprint.service

systemctl status octoprint.service




